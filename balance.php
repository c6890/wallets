<?php

require_once __DIR__ . '/func.php';

/**
 * ETHEREUM
 * 1
 * 0x10E1439455BD2624878b243819E31CfEE9eb721C
 *
 * BSC
 * 56
 * 0x67ef9A8D7f4ed931ac63e873b75d8F8dea64Cdb2 ъ
 *
 * POLYGON
 * 137
 * 0xb37b3b78022E6964fe80030C9161525880274010
 *
 * HARMONY
 * ???
 * 0x50239f0D06636a4Ca97Fb2Ad7125fDDa63E692A4
 *
 * NEAR
 * ??? https://api.app.astrodao.com/api/v1/stats/dao/unchain-fund.sputnik-dao.near/state
 * $data['totalDaoFunds']['value']
 * unchain-fund.sputnik-dao.near
 *
 * AVALANCH
 * 43114
 * 0x0a1e264cDeb531657b7Ff65860B70bE6761DC7aa
 *
 * CELO
 * ???
 * 0xE0899EDB2b7a39a2BEFF4165Cf9f2DbDbf126505
 */

$defaultUrl = 'https://safe-client.gnosis.io';

$wallets = [
    // BSC
    [
        'id' => 'BSC',
        "chain" => "56",
        "key" => "0x67ef9A8D7f4ed931ac63e873b75d8F8dea64Cdb2",
    ],
    // ETHEREUM
    [
        'id' => 'ETHEREUM',
        "chain" => "1",
        "key" => "0x10E1439455BD2624878b243819E31CfEE9eb721C",
    ],
    // AVALANCH
    [
        'id' => 'AVALANCH',
        "chain" => "43114",
        "key" => "0x0a1e264cDeb531657b7Ff65860B70bE6761DC7aa",
    ],
    //POLYGON
    [
        'id' => 'POLYGON',
        "chain" => "137",
        "key" => "0xb37b3b78022E6964fe80030C9161525880274010",
    ],
//    CELO
    [
        'id' => 'CELO',
        "chain" => "42220",
        "key" => "0x99300109Cd3BccC0eE1c535b9bB0b5f4A6a2aDb4",
        'baseUrl' => 'https://client-gateway.celo-safe.io',
    ],
];

$totalUSD = 0;
$walletUSD = [];
foreach ($wallets as $wallet) {
    $baseUrl = $wallet['baseUrl'] ?? $defaultUrl;
    $response = file_get_contents("{$baseUrl}/v1/chains/{$wallet['chain']}/safes/{$wallet['key']}/balances/USD?exclude_spam=true&trusted=false");
    $data = json_decode($response, true);
    $totalUSD += $data['fiatTotal'];
    printf("%s [%s - %s] Got balance %f $\n", date('r'), $wallet['id'], $wallet['chain'], $data['fiatTotal']);
    foreach ($data['items'] as $row) {
        $walletKey = $row['tokenInfo']['symbol'];
        if (!array_key_exists($walletKey, $walletUSD)) {
            $walletUSD[$walletKey] = ['usd' => 0.0, 'fiatBalance' => 0.0, 'fiatConversion' => $row['fiatConversion']];
        }
        $walletUSD[$walletKey]['usd'] += $row['fiatBalance'];
        $walletUSD[$walletKey]['fiatBalance'] += $row['fiatBalance'];
    }

// append outgoing transaction
//https://safe-client.gnosis.io/v1/chains/1/safes/0x10E1439455BD2624878b243819E31CfEE9eb721C/transactions/history
//$data['results'][0]['type'] == 'TRANSACTION' ['transaction']['txStatus'] == 'SUCCESS' ['txInfo']['direction'] == OUTGOING
//['value'] in symbolCoin , ['tokenSymbol']

//        https://safe-client.gnosis.io/v1/chains/56/safes/0x67ef9A8D7f4ed931ac63e873b75d8F8dea64Cdb2/transactions/history?cursor=limit%3D20%26offset%3D20&timezone_offset=0

    $historyUrl = "{$baseUrl}/v1/chains/{$wallet['chain']}/safes/{$wallet['key']}/transactions/history?cursor=limit%3D100%26offset%3D0";
    do {
        $responseHistory = file_get_contents($historyUrl);
        printf("%s [%s] Got transcation history\n", date('r'), $wallet['chain']);
        $dataHistory = json_decode($responseHistory, true);
        foreach ($dataHistory['results'] as $row) {
            if ($row['type'] == 'TRANSACTION') {
                $transaction = $row['transaction'];

                $transactionDate = new DateTime();
                $transactionDate->setTimestamp((int)$transaction["timestamp"] / 1000);

//                if (new DateTime('2022-02-23') > $transactionDate) {
//                    break;
//                }

                if ($transaction['txStatus'] == 'SUCCESS'
                    and $transaction['txInfo']['type'] == 'Transfer'
                    and $transaction['txInfo']['direction'] == 'OUTGOING') {
                    $symbol = $transaction['txInfo']['transferInfo']['tokenSymbol'];
                    $fiatConversion = (int)$walletUSD[$symbol]['fiatConversion'];
                    $transactionValue = $transaction["txInfo"]["transferInfo"]["value"];
                    $transactionDecimals = $transaction["txInfo"]["transferInfo"]["decimals"];
                    if ($fiatConversion > 0) {
                        $transactionUSD = (int)substr($transactionValue, 0, -1 * $transactionDecimals) * $fiatConversion;
                        $walletUSD[$symbol]['usd'] += $transactionUSD;
//                        $walletUSD[$symbol]['fiatBalance'] += $transactionUSD;
                        $totalUSD += $transactionUSD;
                        printf("%s [%s] Add outgoing %f $\n", date('r'), $symbol, $transactionUSD);
                    } elseif ($symbol == 'USDT') {
                        $transactionUSD = (int)substr($transactionValue, 0, -1 * $transactionDecimals) * 1;
                        $walletUSD[$symbol]['usd'] += $transactionUSD;
//                        $walletUSD[$symbol]['fiatBalance'] += $transactionUSD;
                        $totalUSD += $transactionUSD;
                        printf("%s [%s] Add outgoing USDT %f $\n", date('r'), $symbol, $transactionUSD);
                    }
                }
            }
        }
        $historyUrl = (string)@$dataHistory['next'];
    } while ($historyUrl);
}


// get data from NEAR
$nearWallet = 'unchain-fund.sputnik-dao.near';
//$nearBalanceUSD = getNearBalanceUSD($nearWallet); // old
//printf("Near balance: %f $", $nearBalanceUSD);
$nearWalletUSD = getNearWalletUSD($nearWallet);
foreach ($nearWalletUSD as $symbol => $row) {
    $walletUSD[$symbol] = $row;
    if ('NEAR' == $symbol) {
        $nearOutgoingUSD = getNearOutgoingUSD($nearWallet, $row['fiatConversion']);
        printf("Near outgoing [%s]: %f $", $symbol, $nearOutgoingUSD);
        $walletUSD[$symbol]['usd'] += $nearOutgoingUSD;
    }
    $totalUSD += $walletUSD[$symbol]['usd'];
}

//polkadot
$polkadotWallet = '16cQzDgjNYyobLBFA7jct5s8UJp1czxkZMJ2o8paqNs1ECg8';
$polkadotBalanceUsd = getPolkadotBalanceUSD($polkadotWallet);
printf("Poladot balance: %f $", $polkadotWallet);
$walletUSD['POLKADOT'] = ['usd' => $polkadotBalanceUsd];
$totalUSD += $polkadotBalanceUsd;


renderJson($totalUSD, $walletUSD);
renderHtml($totalUSD, $walletUSD);

